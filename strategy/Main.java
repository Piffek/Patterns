package com.company;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        List<Lesson> lessons = new ArrayList<>();
        lessons.add(new Seminar(4, new FixedCostStrategy()));
        lessons.add(new Seminar(4, new TimeCostStrategy()));

        for (Lesson les : lessons){
            System.out.println("Cost: "+les.cost()+" Type:"+les.chargeType());
        }
    }
}

abstract class CostStrategy
{
    abstract int cost(Lesson lesson);
    abstract String chargeType();
}

class Lesson
{
    private int duration;
    private CostStrategy costStrategy;

    Lesson(int duration, CostStrategy costStrategy){
        this.duration = duration;
        this.costStrategy = costStrategy;
    }

    public String changeType(){
        return this.costStrategy.chargeType();
    }

    public int cost(){
       return this.costStrategy.cost(this);
    }

    public int getDuration() {
        return duration;
    }
}

class Lecture extends Lesson
{

    Lecture(int duration, CostStrategy costStrategy) {
        super(duration, costStrategy);
    }
}

class Seminar extends Lesson
{
    public Seminar(int duration, CostStrategy costStrategy) {
        super(duration, costStrategy);
    }
}

class FixedCostStrategy extends CostStrategy
{

    @Override
    int cost(Lesson lesson) {
        return 30;
    }

    @Override
    String changeType() {
        return "stawka stała";
    }
}

class TimeCostStrategy extends CostStrategy
{

    @Override
    int cost(Lesson lesson) {
        return (lesson.getDuration() * 10);
    }

    @Override
    String changeType() {
        return "stawka czasowa";
    }
}

